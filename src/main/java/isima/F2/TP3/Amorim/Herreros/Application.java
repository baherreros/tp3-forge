package isima.F2.TP3.Amorim.Herreros;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class Application implements ApplicationRunner {

	@Autowired
	private FeeService feeService;
	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args)
	{
		SpringApplication.run(Application.class, args);
		log.warn("Hello Word");
		log.debug(StringUtils.reverse("Herreros Amorim"));
	}

	@Override
	public void run(ApplicationArguments args)
	{

		if(args.getSourceArgs().length > 0)
		{
			try {

				Long km = Long.parseLong(args.getSourceArgs()[0]);

				log.info("input sanitized");

				log.info(String.format("For %ld km, fee is %lf !", km, feeService.computeFee(km)));

			} catch (java.lang.NumberFormatException e)
			{
				log.info("Wrong arg");
			}

		}


	}


}
