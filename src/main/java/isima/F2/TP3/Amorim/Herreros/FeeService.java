package isima.F2.TP3.Amorim.Herreros;


import org.springframework.stereotype.Component;

@Component
public class FeeService
{
    public double computeFee(Long km) throws java.lang.NumberFormatException
    {
        if(km < 0)
        {
            throw new java.lang.NumberFormatException();
        }

        Long[] bornes = {10L ,39L, 60L, Long.MAX_VALUE};
        double[] tarif = {1.50, 0.40, 0.55, 6.81};
        int tranche = 0;
        Long nbTranches = km;


        while(km > bornes[tranche])
        {
            ++tranche;
        }

        if(tranche >= 3)
        {
            nbTranches = (km / 20) - 2;
        }

        return nbTranches * tarif[tranche];

    }
}
