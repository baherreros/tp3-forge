package isima.F2.TP3.Amorim.Herreros;


import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ApplicationTests {

	@Autowired
	FeeService feeService;

	/* intervalles : 1 10 |	39|40 60 80 100 ...*/
	/* arrondis au superieur 0.5 = 0, 0.51 = 1 */

	/*cas à tester :
		5, 15, 45, 65

		1, 10, 40, 60

		0, 81

		001, 'a'
	*/
	@Test
	void testInf10()
	{
		//valeur normale
		Assert.assertEquals(1.50 * 5, feeService.computeFee(5L), 0);

		//limites
		Assert.assertEquals(1.50, feeService.computeFee(1L), 0);
		Assert.assertEquals(1.50 * 10, feeService.computeFee(10L), 0);
	}

	@Test
	void testInf39()
	{
		//valeur normale
		Assert.assertEquals(0.40 * 15, feeService.computeFee(15L), 0.01);

		//limites

		Assert.assertEquals(0.40 * 11, feeService.computeFee(11L), 0.01);
		Assert.assertEquals(0.40 * 39, feeService.computeFee(39L), 0.01);

	}

	@Test
	void testInf60()
	{
		//valeur normale
		Assert.assertEquals(0.55 * 45, feeService.computeFee(45L), 0.01);

		//limites

		Assert.assertEquals(0.55 * 40, feeService.computeFee(40L), 0.01);
		Assert.assertEquals(0.55 * 60, feeService.computeFee(60L), 0.01);

	}


	@Test
	void testSup60()
	{
		/*valeurs normales*/

		Assert.assertEquals(6.81, feeService.computeFee(65L), 0.01);
		Assert.assertEquals(2 * 6.81, feeService.computeFee(85L), 0.01);

		//valeurs aux limites

		Assert.assertEquals(6.81, feeService.computeFee(61L), 0.01);
		Assert.assertEquals(2 * 6.81, feeService.computeFee(80L), 0.01);

		Assert.assertEquals(2 * 6.81, feeService.computeFee(89L), 0.01);

	}


	@Test
	void testOther()
	{
		boolean expect = false;

		try
		{
			feeService.computeFee(-10L);
		}
		catch(java.lang.NumberFormatException e)
		{
			expect = true;
		}

		Assert.assertTrue(expect);

	}


}
